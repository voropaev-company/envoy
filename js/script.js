$(window).on('load', function () {
    AOS.init({
        duration: 750
    });
});
$(document).ready(function () {
    $('.btn-menu').on('click', function () {
        $(this).toggleClass('open');
        $('.header-nav').toggleClass('open').fadeToggle();
        $('body').toggleClass('menu-open');
    });

    var $document = $(document);
    var $header = $('header');

    function onScroll(e) {
        var scrollPos = $document.scrollTop();
        if (scrollPos > $header.height()/2) {
            $header.addClass('scroll');
        } else {
            $header.removeClass('scroll');
        }
    }
    onScroll();
    $document.on("scroll", onScroll);

    $('.scroll-page').on('click', function (e) {
        e.preventDefault();
        var body = $('html, body');
        var $target = $($(this).attr('href'));
        if ($target.length) {
            $('html, body').animate({
                'scrollTop': $target.offset().top - 60
            }, 500);
        }
        if ($('.btn-menu').hasClass('open')) $('.btn-menu').removeClass('open');
        if ($('.header-nav').hasClass('open')) $('.header-nav').removeClass('open').fadeOut();
        if ($('body').hasClass('menu-open')) $('body').removeClass('menu-open');

        return false;
    });
    $('.open-team').on('click', function () {
        $('.section-team-content-team-body').slideToggle();
    });

    var video = document.getElementById("myVideoPlayer");
    function startVideo(){
        video.play();
    }
    $(".play").on('click', function(){
        video.play();
    });
    function stopVideo(){
        video.pause();
    }
    $(".stop").on('click', function(){
        stopVideo();
    });
});
